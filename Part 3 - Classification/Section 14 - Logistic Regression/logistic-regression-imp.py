#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 22:09:57 2018

@author: melaniehsu
"""

# Logistic Regression
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Social_Network_Ads.csv')
X = dataset.iloc[:, [ 2, 3]].values
y = dataset.iloc[:, 4].values

# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.25, random_state = 0)

# Feature Scaling - Age, Salary are on different scales. y_train already 0-1.
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

# Fit logistic regression to the training set - import linear model library,
# as logistic regression is a linear classifier - in 2D, two categories of 
# users will be separated by a straight line.
from sklearn.linear_model import LogisticRegression
classifier = LogisticRegression(random_state = 0) # Create logistic regression object
# Fit the logistic regression classifier model to the training set, so that
# the classifier learns the correlations between X_train and y_train, then use
# this knowledge to predict new observations.
classifier.fit(X_train, y_train) 

# Predict test set results
y_pred = classifier.predict(X_test)

# Making the Confusion Matrix - evalute if model learned/understood correlations
# in the training set, and can make accurate predictions on a new set
from sklearn.metrics import confusion_matrix #class - Capital, this is a function
cm = confusion_matrix(y_test, y_pred)

# Red points - 0, Green points - 1; plot the training set results
from matplotlib.colors import ListedColormap # colorize data points
X_set, y_set = X_train, y_train

# Prepare grid with pixel points. Take min of the age values - 1 max of the age
# values + 1, so that the points are not squeezed on the axis. Same for salary.
# steps = resolution; if resolution were higher, you would see the pixel points.
X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01), 
                     np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))

# Apply the classifier on all pixel observation points, by doing that, it 
# colorizes red, green pixel points. Use contour function to make the contour
# between the two regions. Use predict function of classifier to predict 
# whether the pixel point belongs to class 0 (red) or class 1 (green).
plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
             alpha = 0.75, cmap = ListedColormap(('red', 'green')))

# Plot limits of age, salary
plt.xlim(X1.min(), X1.max())
plt.ylim(X2.min(), X2.max())

# Plot all data points (real values)
for i, j in enumerate(np.unique(y_set)):
    plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, i], 
                c = ListedColormap(('red', 'green'))(i), label = j)

plt.title('Logistic Regression (Training set)')
plt.xlabel('Age')
plt.ylabel('Estimated Salary')
plt.legend()
plt.show()

# Red points - 0, Green points - 1; plot the test set results
from matplotlib.colors import ListedColormap # colorize data points
X_set, y_set = X_test, y_test

# Prepare grid with pixel points. Take min of the age values - 1 max of the age
# values + 1, so that the points are not squeezed on the axis. Same for salary.
# steps = resolution; if resolution were higher, you would see the pixel points.
X1, X2 = np.meshgrid(np.arange(start = X_set[:, 0].min() - 1, stop = X_set[:, 0].max() + 1, step = 0.01), 
                     np.arange(start = X_set[:, 1].min() - 1, stop = X_set[:, 1].max() + 1, step = 0.01))

# Apply the classifier on all pixel observation points, by doing that, it 
# colorizes red, green pixel points. Use contour function to make the contour
# between the two regions. Use predict function of classifier to predict 
# whether the pixel point belongs to class 0 (red) or class 1 (green).
plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
             alpha = 0.75, cmap = ListedColormap(('red', 'green')))

# Plot limits of age, salary
plt.xlim(X1.min(), X1.max())
plt.ylim(X2.min(), X2.max())

# Plot all data points (real values)
for i, j in enumerate(np.unique(y_set)):
    plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, i], 
                c = ListedColormap(('red', 'green'))(i), label = j)

plt.title('Logistic Regression (Test set)')
plt.xlabel('Age')
plt.ylabel('Estimated Salary')
plt.legend()
plt.show()