#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 20:44:47 2018

@author: melaniehsu

"""

# math tools
import numpy as np 
# import sub-library, helps with chart plotting
import matplotlib.pyplot as plt 
# import and manage datasets
import pandas as pd 

# import the dataset
dataset = pd.read_csv('Data.csv')

# create matrix of all rows & all but last column of the dataset (indep vars.)
X = dataset.iloc[:, :-1].values

# create vector of the dependent var.
y = dataset.iloc[:, 3].values

# scikitlearn preprocessing library - helps pre-process data set
# Imputer to take care of missing data - impute means inferring missing data
# using known parts of the data
from sklearn.preprocessing import Imputer

# replace missing values with mean along axis
imputer = Imputer(missing_values = 'NaN', strategy = 'mean', axis = 0)

# fit imputer object to matrix of features, X - this is the training data for
# computing the statistics, it gets stored then used on test data during the
# transform phase; get columns with indexes 1 & 2, the ones with missing values
imputer = imputer.fit(X[:, 1:3])

# replace missing data by the mean of the column
X[:, 1:3] = imputer.transform(X[:, 1:3])

# encode categorical data 
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X = LabelEncoder()

# Fit label encoder to the Country column of X, this returns the encoded col
X[:, 0] = labelencoder_X.fit_transform(X[:, 0])

# Dummy variable - encode each country in its own column (1 = that country, 
# 0 = other country), so that ML model doesn't attribute order into the 
# categorical variables.

# Specify which column to one hot encode (the category)
onehotencoder = OneHotEncoder(categorical_features = [0])
X = onehotencoder.fit_transform(X).toarray()

# ML model knows that dependent variable is a category, and there is no order
# between the two. Need to create new object, as the labelencoder above is 
# fitted to X.
labelencoder_Y = LabelEncoder()
y = labelencoder_Y.fit_transform(y)

# Split data into training and test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, 
                                                    random_state = 0)

# Feature scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

