#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 21:05:28 2018

@author: melaniehsu
"""

# Data Preprocessing Template

# Importing the libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# Importing the dataset
dataset = pd.read_csv('Salary_Data.csv')
X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 1].values

# Splitting the dataset into the Training set and Test set
from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1/3, random_state = 0)

# Feature Scaling - commented out as the linear regression takes care of it
"""from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)
sc_y = StandardScaler()
y_train = sc_y.fit_transform(y_train)"""

# Fit simple linear regression to the training set
# - model learns correlations to make future predictions
from sklearn.linear_model import LinearRegression #import class from library
regressor = LinearRegression() # function returning object of itself
regressor.fit(X_train, y_train) # fit regressor object to training set

# regressor has learned the correlation between the indep, dep vars.
# Predict test set results, y_test contains the real values
y_pred = regressor.predict(X_test) # vector of predictions of dependent variables

# Visualize the training set results - compare the real vs. predicted salaries
# of the training set
plt.scatter(X_train, y_train, color = 'red') # real values
plt.plot(X_train, regressor.predict(X_train), color = 'blue') # predicted values
plt.title('Salary vs Experience (Training set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.show()

# Visualize test set results
plt.scatter(X_test, y_test, color = 'red') # test set observations, real values
# regression line would be the same even if you used X_test, since we apply the 
# model that was trained on the training set to make the predictions
plt.plot(X_train, regressor.predict(X_train), color = 'blue') 
plt.title('Salary vs Experience (Test set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.show()