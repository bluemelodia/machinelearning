#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  4 17:46:00 2018

@author: melaniehsu
"""

# Polynomial Regression - non-linear regressor. Although it fits a nonlinear 
# model to the data, as a statistical estimation problem it is linear, in the 
# sense that the regression function E(y|x) is linear in the unknown parameters
# that are estimated from the data. For this reason, polynomial regression is 
# considered to be a special case of multiple linear regression.

import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 

# Don't need position column as it's encoded in the level.
dataset = pd.read_csv('Position_Salaries.csv')
# Have X considered as matrix, not vector.
X = dataset.iloc[:, 1:2].values 
y = dataset.iloc[:, 2].values

# No training/test set or feature scaling here.
# Do not split the data set if you do not have a lot of information.
# Plus, we need as much info as possible to make an accurate prediction.
"""from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, 
                                                    random_state = 0)"""
                                   
# Polynomial regression consists of adding polynomial terms, we'll be using 
# the same linear regression library as simple/multiple lienar regression, 
# which takes care of the feature scaling.

# Fit linear regression to the dataset
from sklearn.linear_model import LinearRegression
lin_reg = LinearRegression()
lin_reg.fit(X, y)

# Fit polynomial regression to the dataset
from sklearn.preprocessing import PolynomialFeatures
# Transforms matrix of features, X, into a new matrix of features containing 
# all polynomial combinations of the features with degree <= specified degree.
poly_reg = PolynomialFeatures(degree = 4)

# Fit object to X, and transform X -> X_poly (1, x, x^2)
# poly_reg automatically includes the b0 constant, in case we need to use 
# libs that require the columns of 1s in the matrix of features
X_poly = poly_reg.fit_transform(X)
           
# Create the polynomial model                                         
lin_reg_2 = LinearRegression()
lin_reg_2.fit(X_poly, y)

# Visualize the linear regression results
plt.scatter(X, y, color = 'red')
plt.plot(X, lin_reg.predict(X), color = 'blue')
plt.title('Truth or Bluff (Linear Regression)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()

# Visualize the polynomial regression results

# Create evenly spaced values within the X space, outputting an array
X_grid = np.arange(min(X), max(X), 0.1)

# Reshape the array into a matrix
X_grid = X_grid.reshape((len(X_grid), 1))

plt.scatter(X, y, color = 'red')

# Do not use X_poly, otherwise code won't be reusable for future matrices.
plt.plot(X, lin_reg_2.predict(poly_reg.fit_transform(X)), color = 'blue')
plt.title('Truth or Bluff (Polynomial Regression)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()

# Predict a new result with linear regression 
# HR says that employee falls under level 6.5, what salary should they get?
lin_reg.predict(6.5)

# Predict a new result with polynomial regression 
lin_reg_2.predict(poly_reg.fit_transform(6.5))