#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul  7 20:36:22 2018

@author: melaniehsu
"""

import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 

# Import dataset
dataset = pd.read_csv('Position_Salaries.csv')
X = dataset.iloc[:, 1:2].values 
y = dataset.iloc[:, 2].values

# Split the dataset into training and test set
"""from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, 
                                                    random_state = 0)"""
                                   
# Feature scaling
from sklearn.preprocessing import StandardScaler
sc_X = StandardScaler()
sc_y = StandardScaler()
# Each object is fitted to different matrix, so X, y need their own scalers
X = sc_X.fit_transform(X)
y = sc_y.fit_transform(y.reshape(-1, 1))

# Fit SVR (support vector machine for regression) to the dataset
from sklearn.svm import SVR
# Create SVR regressor
# Kernel argument - our data is non-linear, so don't use linear. Gaussian/RBF
# is the most common choice (default). 

# SVR might not fit outlier observations well and not fit to it, as it's 
# using penalty parameters in its algorithm. 
regressor = SVR(kernel = 'rbf')
regressor.fit(X, y)

# Predict a new result - apply sc_X object used to scale features to 6.5, 
# so that it is suited to the regressor. Don't need to fit, since object
# is already fit to matrix of features, X. Double brackets - array of one
# row, one column. With just [], it'd be a single-element vector -> data error
# due to parameter type mismatch.
y_pred = regressor.predict(sc_X.transform(np.array([[6.5]])))

# Then inverse transform to get the original salary. 
y_pred = sc_y.inverse_transform(y_pred)

# Visualize the SVR results
plt.scatter(X, y, color = 'red')
plt.plot(X, regressor.predict(X), color = 'blue')
plt.title('Truth or Bluff (SVR)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()

# Visualize the SVR results (higher resolution and smoother curve)
X_grid = np.arange(min(X), max(X), 0.1)
X_grid = X_grid.reshape((len(X_grid), 1))
plt.scatter(X, y, color = 'red')
plt.plot(X_grid, regressor.predict(X_grid), color = 'blue')
plt.title('Truth or Bluff (SVR)')
plt.xlabel('Position level')
plt.ylabel('Salary')
plt.show()